package com.saxo_wrapper.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ReactiveHttpOutputMessage;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserter;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.json.*;

@Service
public class HttpService {
    @Autowired
    private WebClient webClient;

    public void getRequest() {
        String apiKey = "d7eac20cd286fa279a64b7ec11458c1127967724";
        String body = "{\"identifier\":\"Stingray\", \"password\":\"Corv011969\"}";

        String mapFlux = webClient
                .post()
                .contentType(MediaType.APPLICATION_JSON)
                .header("X-IG-API-KEY", apiKey)
                .header("IG-ACCOUNT-ID", "Z3RRN8")
                .body(BodyInserters.fromValue(body))
                .retrieve()
                .bodyToMono(String.class).block();
        System.out.println(mapFlux);
    }
}

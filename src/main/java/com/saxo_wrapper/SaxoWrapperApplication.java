package com.saxo_wrapper;

import com.saxo_wrapper.service.HttpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ApplicationContext;

import javax.el.BeanNameResolver;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class SaxoWrapperApplication {
    
    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(SaxoWrapperApplication.class, args);
        HttpService service = applicationContext.getBean(HttpService.class);
        service.getRequest();
    }
}

package com.saxo_wrapper.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class HttpClientConfig {
    @Autowired
    private Environment env;

    @Bean
    public WebClient createHttpClient() {
        String url ="/session";
        return WebClient.create(env.getProperty("base_url") + url);
    }
}
